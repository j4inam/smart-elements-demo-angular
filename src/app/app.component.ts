import { Component, AfterViewInit, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Orientation } from "smart-webcomponents-angular/smart.elements";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit, AfterViewInit {
  valueUpdateInterval;
  orientationOptions = [
    {
      viewValue: "Vertical",
      value: Orientation.Vertical
    },
    {
      viewValue: "Horizontal",
      value: Orientation.Horizontal
    }
  ];

  coerce = true;
  tankConfigurationForm = new FormGroup({
    min: new FormControl(0, []),
    max: new FormControl(100, []),
    value: new FormControl(35, []),
    interval: new FormControl(10, []),
    orientation: new FormControl(Orientation.Vertical, []),
    readonly: new FormControl(true, []),
  });

  ngOnInit() {
    this.tankConfigurationForm.get("interval").disable();
  }

  ngAfterViewInit() {
    this.randomizeValues();
  }

  randomizeValues() {
    let value: number;
    this.valueUpdateInterval = setInterval(() => {
      value = Math.floor((Math.random() * 1000) % 100);
      this.tankConfigurationForm.get("value").patchValue(value);
    }, 2000);
  }

  onReadOnlyChanged() {
    clearInterval(this.valueUpdateInterval);
    if (this.tankConfigurationForm.value.readonly) {
      this.randomizeValues();
      this.tankConfigurationForm.get("interval").disable();
    } else {
      this.tankConfigurationForm.get("interval").enable();
    }
  }

  viewGitlabRepo() {
      window.open("https://gitlab.com/j4inam/smart-elements-demo-angular", "__blank");
  }

}
